package ru.iteco.taskmanager.enumerate;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;

public enum ReadinessStatus {

	ID_1 ("Planned"),
	ID_2 ("During"),
	ID_3 ("Ready");
	
	@NotNull
	@Getter
	private String displayName;

	ReadinessStatus(final String displayName) {
		this.displayName = displayName;
	}
	
	@NotNull
	@Override
	public String toString() {
		return displayName;
	}
}
