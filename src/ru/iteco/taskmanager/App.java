package ru.iteco.taskmanager;

import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;

import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.command.AbstractCommand;

public class App
{
	@NotNull
	private static final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.taskmanager").getSubTypesOf(AbstractCommand.class);
	
	public static void main(final String[] args )
    {
    	@NotNull
    	final Bootstrap bootstrap = new Bootstrap();
    	bootstrap.init(classes);
    }
}
