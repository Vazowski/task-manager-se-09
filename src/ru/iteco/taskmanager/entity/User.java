package ru.iteco.taskmanager.entity;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.RoleType;

public final class User extends AbstractEntity {

	@NotNull
	@Getter 
	@Setter
	private String login;
	@NotNull
	@Getter 
	@Setter
	private String passwordHash;
	@NotNull
	@Getter 
	@Setter
	private RoleType roleType;
	
	public User() {
		
	}
	
	public User(@NotNull String login, @NotNull String passwordHash, @NotNull RoleType roleType, @NotNull String uuid) {
		this.uuid = uuid;
		this.login = login;
		this.passwordHash = passwordHash;
		this.roleType = roleType;
	}
}
