package ru.iteco.taskmanager.entity;

import java.util.Date;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;

public final class Task extends AbstractEntity  {

	@NotNull
	@Getter 
	@Setter
	private String ownerId;
	@NotNull
	@Getter 
	@Setter
	private String projectUUID;
	@NotNull
	@Getter 
	@Setter
	private String name;
	@NotNull
	@Getter 
	@Setter
	private String description;
	@NotNull
	@Getter 
	@Setter
	private String dateCreated;
	@NotNull
	@Getter 
	@Setter
	private String dateBegin;
	@NotNull
	@Getter 
	@Setter
	private String dateEnd;
	@NotNull
	@Getter 
	@Setter
	private String type;
	@NotNull
	@Getter 
	@Setter
	private ReadinessStatus readinessStatus;
	
	public Task() {
    	
    }
    
    public Task(@NotNull String name, @NotNull String description, @NotNull String uuid, @NotNull String projectUuid, @NotNull String ownerId, @NotNull final String dateBegin, @NotNull final String dateEnd) {
    	this.ownerId = ownerId;
    	this.uuid = uuid;
    	this.projectUUID = projectUuid;
    	this.name = name;
    	this.description = description;
    	this.dateCreated = dateFormat.format(new Date());
    	this.dateBegin = dateBegin;
    	this.dateEnd = dateEnd;
    	this.type = "Task";
    	this.readinessStatus = ReadinessStatus.ID_1;
    }
	
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateCreated: " + this.dateCreated + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid + "\nprojectUUID: " + this.projectUUID;
	}
}
