package ru.iteco.taskmanager.api.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Task;

public interface ITaskService {

	void merge(final String name, final String description, final String uuid, final String projectUuid, final String ownerId, final String dateBegin, final String dateEnd);
	void remove(final String name);
	void removeAllByProjectUuid(final String uuid);
	void removeAll();
	Task findByUuid(final String uuid);
	Task findByName(final String name);
	List<Task> findAll(final String projectUuid);
	List<Task> findAll(final String projectUuid, final String ownerId);
	List<Task> findAllByPartName(@NotNull final String ownerId, @NotNull final String partOfName);
	List<Task> findAllByPartDescription(@NotNull final String ownerId, @NotNull final String partOfDescription);
}
