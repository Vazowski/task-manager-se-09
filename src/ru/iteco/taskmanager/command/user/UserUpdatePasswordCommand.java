package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.User;

public final class UserUpdatePasswordCommand extends AbstractCommand {

	@Override
	public String command() {
		return "user-new-password";
	}

	@Override
	public String description() {
		return "  -  create new password for user";
	}

	@Override
	public void execute() throws Exception {
		while(true) {
			System.out.print("Current password: ");
			@NotNull 
			String password = scanner.nextLine();
			@NotNull 
			final User tempUser = serviceLocator.getUserService().findByLogin(serviceLocator.getUserService().getCurrent());
			if (!tempUser.getPasswordHash().equals(serviceLocator.getUserService().getPassword(password))) {
				System.out.println("Entered password is incorrect");
				break;
			}
			System.out.print("New password: ");
			password = scanner.nextLine();
			serviceLocator.getUserService().merge(tempUser.getLogin(), serviceLocator.getUserService().getPassword(password));
			System.out.println("Done");
			break;
		}
	}
}
